/* ==========================================================================
   File: gruvbox-common.rasi
   Desc: Shared rules between all gruvbox themes
   Author: bardisty <b@bah.im>
   Source: https://github.com/bardisty/gruvbox-rofi
   Modified: Mon Feb 12 2018 06:06:47 PST -0800
   ========================================================================== */

configuration {
    modi: "drun";
    font: "Iosevka Term 11";
    show-icons: true;
    display-drun: "  Search";
    drun-display-format: "{name}";
    sidebar-mode: false;
}

@import "gruvbox-dark.rasi"

window {
    background-color: @background;
    border:           2;
    padding:          0;
	width:			42%;
    height:         32%;
    location:                       west;
    x-offset:                       0;
    y-offset:                       0;
}

mainbox {
    border:  0;
    padding: 0;
}

message {
    border:       2px 0 0;
    border-color: @separatorcolor;
    padding:      1px;
}

textbox {
    highlight:  @highlight;
    text-color: @foreground;
}

listview {
    columns: 1;
    lines: 3;
    border:       2px solid 0 0;
    padding:      31px 0 0;
    border-color: @separatorcolor;
    spacing: 0%;
    cycle: false;
    dynamic: true;
    layout: vertical;
}

element {
    orientation: vertical;
    border:  0;
    padding: 2.9% 0% 2.86% 3%;
}

element.normal.normal {
    background-color: @normal-background;
    text-color:       @normal-foreground;
}

element.normal.urgent {
    background-color: @urgent-background;
    text-color:       @urgent-foreground;
}

element.normal.active {
    background-color: @active-background;
    text-color:       @active-foreground;
}

element.selected.normal {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}

element.selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}

element.selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}

element.alternate.normal {
    background-color: @alternate-normal-background;
    text-color:       @alternate-normal-foreground;
}

element.alternate.urgent {
    background-color: @alternate-urgent-background;
    text-color:       @alternate-urgent-foreground;
}

element.alternate.active {
    background-color: @alternate-active-background;
    text-color:       @alternate-active-foreground;
}

element-icon {
    size: 48px;
    border: 0px;
    horizontal-align: 0.5;
}

element-text {
   expand: true;
   horizontal-align: 0.5;
   vertical-align: 0.5;
   margin: 0.5% 0.5% -0.5% 0.5%;
}

element-text, element-icon{
    background-color: inherit;
    text-color: inherit;
}

scrollbar {
    width:        0px;
    border:       0;
    handle-color: @scrollbar-handle;
    handle-width: 0px;
    padding:      0;
}

sidebar {
    border:       0px 0 0;
    border-color: @separatorcolor;
}

inputbar {
    spacing:    0;
    text-color: @normal-foreground;
    padding:    2px;
    children:   [ prompt, textbox-prompt-sep, entry, case-indicator ];
}

case-indicator,
entry,
prompt,
button {
    spacing:    0;
    text-color: @normal-foreground;
}

button.selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}

textbox-prompt-sep {
    expand:     false;
    str:        ":";
    text-color: @normal-foreground;
    margin:     0 0.3em 0 0;
}

